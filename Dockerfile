FROM python:3.8.5-slim

ADD ./requirements.txt /usr/src/gitlab-ci-series/requirements.txt

RUN pip install -r /usr/src/gitlab-ci-series/requirements.txt
